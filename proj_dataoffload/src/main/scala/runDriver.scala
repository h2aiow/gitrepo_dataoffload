import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext


object runDriver {
  
  def main(args:Array[String]){
   // Mysql server details 
  //val args = List("jdbc:mysql://184.168.115.185:3306", "h2adev", "h2a123", "com.mysql.jdbc.Driver", 1,"h2amysqldb.table1", "test-app")
  
    //cloudera mysql
   //val args = List("jdbc:mysql://192.168.0.111:3306", "jdbcuser2", "cloudera", "com.mysql.jdbc.Driver", 1,"retail_db.emp", "test-app")
    
  val conf = new SparkConf().setAppName(args(6).toString()).setMaster("local[*]")
  
  val sc = new SparkContext(conf)
  
  val spark = SparkSession
  .builder()
  .appName(args(6).toString())
  .getOrCreate()
  
  //val sqlContext = new SQLContext(sc)
  //bbgg
  // Added for testing 
   
  val db_conn_string = args(0).toString()
  val username = args(1).toString()
  val pwd = args(2).toString()
  val driver_nm = args(3).toString()
  val sno =args(4).toString()
  val tablename = args(5).toString()
  //val output_dir = args(7).toString()
  val app_name = args(6).toString()
  
  
  val dataframe_mysql2 = spark.sqlContext.read.format("jdbc").option("url", db_conn_string).option("user", username).option("password", pwd).option("driver",driver_nm)
    
    val df = dataframe_mysql2.option("dbtable", tablename).load()
    
    println("count === "+df.count())
  
  }
  
  
}